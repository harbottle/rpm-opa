# rpm-opa

[Open Policy Agent](https://github.com/open-policy-agent/opa) RPM packages and dedicated COPR yum repo.

- [Spec files](https://gitlab.com/harbottle/harbottle-main/-/tree/master/specs)
- [COPR homepage](https://copr.fedorainfracloud.org/coprs/harbottle/opa/)

## Suggested Basic Installation Procedure for CentOS 8

The following script installs the latest release of Open Policy Agent on CentOS 8.

```bash
#!/bin/bash

# Install Open Policy Agent using COPR repo
dnf -y install dnf-plugins-core
dnf config-manager --add-repo https://copr.fedorainfracloud.org/coprs/harbottle/opa/repo/epel-8/harbottle-opa-epel-8.repo
dnf -y install opa
```

## Suggested Basic Installation Procedure for CentOS 7

The following script installs the latest release of Open Policy Agent on CentOS 7.

```bash
#!/bin/bash

# Install Open Policy Agent using COPR repo
yum -y install yum-utils
yum-config-manager --add-repo https://copr.fedorainfracloud.org/coprs/harbottle/opa/repo/epel-7/harbottle-opa-epel-7.repo
yum -y install opa
```
