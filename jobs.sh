#!/bin/bash
ref="${1:-master}"
rm -f jobs.yml
rm -rf archive
mkdir -p archive
pushd archive
wget https://gitlab.com/harbottle/harbottle-main/-/archive/${ref}/harbottle-main-${ref}.tar.gz
tar xzf harbottle-main-${ref}.tar.gz
popd

for i in archive/**/specs/opa*.spec; do
  name=$(basename $i .spec)
  cat <<-EOF >> jobs.yml
  srpm:el7:${name}:
    extends: .srpm_el7

  copr:el7:${name}:
    extends: .copr_el7
    dependencies:
      - get
      - srpm:el7:${name}
    only:
      refs:
        - master@harbottle/rpm-opa


EOF

  cat <<-EOF >> jobs.yml
  srpm:el8:${name}:
    extends: .srpm_el8

  copr:el8:${name}:
    extends: .copr_el8
    dependencies:
      - get
      - srpm:el8:${name}
    only:
      refs:
        - master@harbottle/rpm-opa


EOF

done
